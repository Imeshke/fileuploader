package com.s2t.CameraAppK.ui.adapters

import android.R.drawable
import android.content.Context
import android.media.ThumbnailUtils
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.s2t.CameraAppK.R
import com.s2t.CameraAppK.data.model.FileItem
import com.squareup.picasso.Picasso
import com.squareup.picasso.Picasso.LoadedFrom
import com.squareup.picasso.Request
import com.squareup.picasso.RequestHandler
import java.lang.reflect.Field


class FileAdapter   (val mContext: Context, val fileList: ArrayList<FileItem>, val clickListener:(FileItem) ->Boolean) : RecyclerView.Adapter<FileAdapter.ViewHolder>()  {
    var videoRequestHandler: VideoRequestHandler? = null
    var picassoInstance: Picasso? = null
    //this method is returning the view for each item in the list
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FileAdapter.ViewHolder {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.recycle_item, parent, false)
        videoRequestHandler = VideoRequestHandler()
        picassoInstance = Picasso.Builder(mContext)
            .addRequestHandler(videoRequestHandler!!)
            .build()
        return ViewHolder(v)
    }
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        picassoInstance?.let { holder.bindItems(fileList[position], clickListener, it) }

    }

    //this method is giving the size of the list
    override fun getItemCount(): Int {
        return fileList.size
    }

    //the class is hodling the list view
    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun bindItems(fileItem: FileItem, clickListener: (FileItem) -> Boolean, picassoInstance: Picasso) {
            val textViewName = itemView.findViewById(R.id.file_name) as TextView
            val thumb = itemView.findViewById(R.id.image_view) as ImageView
            val card = itemView.findViewById(R.id.card) as CardView

            textViewName.setText(fileItem.name)

          if (fileItem.type.equals("VIDEO")) {
                picassoInstance.load(fileItem.type + ":" + fileItem.file!!.path).into(thumb)
            } else if (fileItem.type.equals("IMAGE")) {
                fileItem.file?.let {
                    Picasso.get().load(it).placeholder(R.drawable.btn_camera_image).into(thumb)
                }
            } else if (fileItem.type.equals("DOCUMENT")) {

              Picasso.get().load(fileItem.thumb).placeholder(R.drawable.note).into(thumb)
            }


            itemView.setOnLongClickListener {
               // card.setBackgroundResource(null)
                clickListener(fileItem)
            }
        }

    }


     class VideoRequestHandler : RequestHandler() {
        var SCHEME_VIDEO = "VIDEO"
        override fun canHandleRequest(data: Request): Boolean {
            val scheme: String = data.uri.scheme.toString()
            return SCHEME_VIDEO == scheme
        }


        override fun load(data: Request, arg1: Int): Result {
            val bm = ThumbnailUtils.createVideoThumbnail(
                data.uri.getPath(),
                MediaStore.Images.Thumbnails.MINI_KIND
            )
            return Result(bm, LoadedFrom.DISK)
        }
    }

}