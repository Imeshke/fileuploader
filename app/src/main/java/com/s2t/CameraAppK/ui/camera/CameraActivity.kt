package com.s2t.CameraAppK.ui.camera

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.app.DatePickerDialog
import android.app.TimePickerDialog
import android.content.*
import android.content.pm.PackageManager
import android.content.res.AssetFileDescriptor
import android.database.Cursor
import android.graphics.Bitmap
import android.location.Location
import android.location.LocationManager
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.*
import android.provider.MediaStore
import android.provider.OpenableColumns
import android.provider.Settings
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.webkit.MimeTypeMap
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.net.toFile
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.google.android.gms.location.*
import com.google.android.material.tabs.TabLayout

import com.s2t.CameraAppK.R
import com.s2t.CameraAppK.data.model.FileItem
import com.s2t.CameraAppK.data.utils.UrlUtils
import com.s2t.CameraAppK.ui.adapters.TabAdapter
import com.s2t.CameraAppK.ui.login.LoginActivity
import com.s2t.CameraAppK.ui.login.hideKeyboard
import kotlinx.android.synthetic.main.activity_camera.*
import okhttp3.*
import okhttp3.MediaType.Companion.toMediaTypeOrNull
import okhttp3.RequestBody.Companion.asRequestBody
import okhttp3.RequestBody.Companion.toRequestBody
import okio.BufferedSink
import okio.Okio
import org.json.JSONException
import org.json.JSONObject
import java.io.*
import java.lang.Exception
import java.text.DateFormat
import java.text.DateFormatSymbols
import java.text.SimpleDateFormat
import java.time.Duration
import java.util.*
import java.util.concurrent.TimeUnit
import okhttp3.RequestBody


class CameraActivity : AppCompatActivity() {

    val PERMISSION_ID = 42
    lateinit var mFusedLocationClient: FusedLocationProviderClient
    lateinit var location : Location
    private var c = Calendar.getInstance()
    lateinit var filename: EditText
    var hashMap : HashMap<String, String>
            = HashMap<String, String> ()
    private var loading : ProgressBar? = null
    var tabLayout: TabLayout? = null
    var viewPager: ViewPager? = null
    val imageFileItems = ArrayList<FileItem>()
    val videoFileItems = ArrayList<FileItem>()
    val documentFileItems = ArrayList<FileItem>()
    var imageToggle: Switch? = null
    var videoToggle: Switch? = null
    private val PREFS_NAME = "cam_shdprf"
    lateinit var sharedPref: SharedPreferences


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_camera)

        val actionBar = supportActionBar

        val policy =
            StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        sharedPref = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)
        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this@CameraActivity)
        getLastLocation()

        filename = findViewById<EditText>(R.id.filename)
        val description: EditText = findViewById<EditText>(R.id.description)
        val datetime: EditText = findViewById<EditText>(R.id.dateandtime)
        loading= findViewById<ProgressBar>(R.id.loading)
        val upload = findViewById<Button>(R.id.upload_btn)

        datetime.setText(SimpleDateFormat("dd MMMM yyyy HH:mm a").format(c.time))
        var timeZone:TimeZone = TimeZone.getTimeZone("UTC")
        var gmtTime : String = c.time.formatTo("yyyy-MM-dd'T'HH:mm:ss",timeZone)

        hashMap.put("datetime", gmtTime)

        datetime.setOnClickListener{
            setDate(datetime)
        }

        tabLayout = findViewById<TabLayout>(R.id.tabLayout)
        viewPager = findViewById<ViewPager>(R.id.viewPager)

        tabLayout!!.addTab(tabLayout!!.newTab().setText("Images"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Videos"))
        tabLayout!!.addTab(tabLayout!!.newTab().setText("Documents"))
        tabLayout!!.tabGravity = TabLayout.GRAVITY_FILL

        val adapter = TabAdapter(this, supportFragmentManager, tabLayout!!.tabCount)
        viewPager!!.adapter = adapter

        viewPager!!.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))

        tabLayout!!.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager!!.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {

            }
            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })



        upload.setOnClickListener{

            loading!!.visibility = View.VISIBLE
            Thread {
                val fileItems = ArrayList<FileItem>()
                fileItems.addAll(imageFileItems)
                fileItems.addAll(videoFileItems)
                fileItems.addAll(documentFileItems)
                if (!fileItems.isNullOrEmpty()) {
                    if(filename.text.toString().equals(null)||filename.text.toString().equals("")){
                        runOnUiThread() {
                            filename.setError("Name can't be blank")
                            loading!!.visibility = View.GONE
                        }
                    }else {
                        var imageURL : String? = ""
                        var videoURL : String? = ""
                        var documentURL : String? = ""
                        for(fileItem:FileItem in fileItems){
                            if(fileItem.type.equals("IMAGE", true)) {
                                if (!imageURL.equals("")) {
                                    imageURL = imageURL + "," + fileItem.file?.let { it1 ->
                                        fileItem.type?.let { it2 -> sendFileToServer(it1, it2) }
                                    }
                                } else {
                                    imageURL = fileItem.file?.let { it1 -> fileItem.type?.let { it2 ->
                                        sendFileToServer(it1,
                                            it2
                                        )
                                    } }
                                }
                            }
                            else if(fileItem.type.equals("VIDEO", true)) {
                                if (!videoURL.equals("")) {
                                    videoURL = videoURL + "," + fileItem.file?.let { it1 ->
                                        fileItem.type?.let { it2 -> sendFileToServer(it1, it2) }
                                    }
                                } else {
                                    videoURL = fileItem.file?.let { it1 -> fileItem.type?.let { it2 ->
                                        sendFileToServer(it1,
                                            it2
                                        )
                                    } }
                                }
                            }
                            else{
                                if (!documentURL.equals("")) {
                                    documentURL = documentURL + "," + fileItem.file?.let { it1 ->
                                        fileItem.type?.let { it2 -> sendFileToServer(it1, it2) }
                                    }
                                } else {
                                    documentURL = fileItem.file?.let { it1 -> fileItem.type?.let { it2 ->
                                        sendFileToServer(it1,
                                            it2
                                        )
                                    } }
                                }
                            }
                        }

                        imageURL?.let { it1 -> hashMap.put("ImageUrls", it1) }
                        videoURL?.let { it1 -> hashMap.put("VideoUrls", it1) }
                        documentURL?.let { it1 -> hashMap.put("FileUrls", it1) }
                        hashMap.put("fileName", filename.text.toString())
                        hashMap.put("description", description.text.toString())
                        if(imageToggle!!.isChecked|| videoToggle!!.isChecked){
                            hashMap.put("IsTagged", "true")
                        }
                        else{
                            hashMap.put("IsTagged", "false")
                        }
                        sendDataToServer()
                    }
                }else{
                    runOnUiThread(){
                        Toast.makeText(this@CameraActivity, "Empty File List..!!", Toast.LENGTH_SHORT).show()
                        loading!!.visibility = View.GONE
                    }

                }
            }.start()
        }
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        // Inflate the menu to use in the action bar
        val inflater = menuInflater
        inflater.inflate(R.menu.toolbar_menu, menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Handle presses on the action bar menu items
        when (item.itemId) {
            R.id.action_logout -> {
                intent = Intent(applicationContext, LoginActivity::class.java)
                intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }


    fun saveShdPrf(KEY_NAME: String, value: String) {
        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putString(KEY_NAME, value)

        editor.commit()
    }

    fun getValueString(KEY_NAME: String): String? {
        return sharedPref.getString(KEY_NAME, null)
    }

    fun Date.formatTo(dateFormat: String, timeZone: TimeZone ): String {
        val formatter = SimpleDateFormat(dateFormat, Locale.getDefault())
        formatter.timeZone = timeZone
        return formatter.format(this)
    }

    fun sendFileToServer(fileItem: File, fileType:String): String? {
        var mimeType : String? = null
        try {
            mimeType = getMimeType(fileItem.absolutePath)
        }catch (e : StringIndexOutOfBoundsException){
            runOnUiThread {
                Toast.makeText(this@CameraActivity, fileItem.name+" : Media type Error, Mime Type invisible", Toast.LENGTH_SHORT).show()
            }
        }
        if(!mimeType.equals(null)) {
            var fileUrl: String? = null
            val client = OkHttpClient()
                .newBuilder().connectTimeout(10, TimeUnit.MINUTES)
                .readTimeout(10, TimeUnit.MINUTES)
                .build()

            var formBody: RequestBody

            if(fileType.equals("IMAGE", true)){
                formBody = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart(
                        "file", fileItem.name,
                        fileItem.asRequestBody(mimeType!!.toMediaTypeOrNull())
                    )
                    .addFormDataPart("processors", "facial_recognition")
                    .build()
            }else if(fileType.equals("VIDEO", true)){
                formBody = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart(
                        "file", fileItem.name,
                        fileItem.asRequestBody(mimeType!!.toMediaTypeOrNull())
                    )
                    .addFormDataPart("processors", "video_indexer")
                    .build()
            }else{
                formBody = MultipartBody.Builder()
                    .setType(MultipartBody.FORM)
                    .addFormDataPart(
                        "file", fileItem.name,
                        fileItem.asRequestBody(mimeType!!.toMediaTypeOrNull())
                    ) //.addFormDataPart("processors", "video_indexer")
                    .build()
            }

            val request: Request =
                Request.Builder().url(UrlUtils.SERVER_POST_FILES).header("Content-Type", "multipart/form-data").post(formBody).build()
            try {
                val response = client.newCall(request).execute()

                try {
                    val myResponse = JSONObject(response.body!!.string())
                    fileUrl = myResponse.getString("fileUrl")
                    var fname  = myResponse.getString("fileName")
                    callAuditTrial(fname)
                    runOnUiThread {
                        Toast.makeText(
                            this@CameraActivity,
                            fileItem.name + " : Upload Success, uploading data...",
                            Toast.LENGTH_SHORT
                        ).show()
                    }

                } catch (e: JSONException) {
                    e.printStackTrace()
                    runOnUiThread {
                        Toast.makeText(
                            this@CameraActivity,
                            fileItem.name + " : Upload fail",
                            Toast.LENGTH_SHORT
                        ).show()
                        loading!!.visibility = View.GONE
                    }
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
            return fileUrl
        }
        return ""
    }

    fun getMimeType( url:String):String {
        val extention: String = url!!.substring(url!!.lastIndexOf("."))
        val mimeTypeMap = MimeTypeMap.getFileExtensionFromUrl(extention)
       val mimeType =
            MimeTypeMap.getSingleton().getMimeTypeFromExtension(mimeTypeMap)
        return mimeType.toString()
    }

    fun View.hideKeyboard() {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(windowToken, 0)
    }

    @RequiresApi(Build.VERSION_CODES.O)
    fun sendDataToServer(){
        var JSON = "application/json; charset=utf-8".toMediaTypeOrNull()

        var filetype4Message: String = null.toString()

        var bodyString : String? = null
        if(!hashMap.get("description").equals("")) {
            bodyString = "{\"Name\":\"" + hashMap.get("fileName") + "\"," +
                    "\"Description\":\"" + hashMap.get("description") + "\"," +
                    "\"_geoLocations\": [\"" + hashMap.get("location") + "\"]," +
                    "\"PublishDate\":\"" + hashMap.get("datetime") + "\""
        }else{
            bodyString = "{\"Name\":\"" + hashMap.get("fileName") + "\"," +
                    "\"_geoLocations\": [\"" + hashMap.get("location") + "\"]," +
                    "\"PublishDate\":\"" + hashMap.get("datetime") + "\""
        }
        if(!imageFileItems.isNullOrEmpty()){
            bodyString = bodyString.plus( ",\"ImageUrls\": [\"" + hashMap.get("ImageUrls") + "\"]" )
            filetype4Message = "Images"
        }
        if(!videoFileItems.isNullOrEmpty()){
            bodyString = bodyString.plus( ",\"VideoUrls\": [\"" + hashMap.get("VideoUrls") + "\"]")
            filetype4Message = "Videos"
        }
        if (!documentFileItems.isNullOrEmpty()){
            bodyString = bodyString.plus(",\"FileUrls\": [\"" + hashMap.get("FileUrls") + "\"]")
            filetype4Message = "Documents"
        }
        bodyString = bodyString.plus(",\"IsTagged\":\"" + hashMap.get("IsTagged") + "\"")
        bodyString = bodyString.plus("}")

        var body = bodyString.toRequestBody(JSON)

        val client = OkHttpClient()
            .newBuilder().connectTimeout(20, TimeUnit.SECONDS)
            .readTimeout(90, TimeUnit.SECONDS)
            .build()

        val request: Request = Request.Builder()
            .url(UrlUtils.SERVER_POST_DATA)
            .post(body)
            //.addHeader("Authorization", "header value") //Notice this request has header if you don't need to send a header just erase this part
            .addHeader("Content-Type", "application/json")
            .build();

        var call = client.newCall(request)
        runOnUiThread {
            Toast.makeText(
                this@CameraActivity,
                filetype4Message + " analysis is started, this might take awhile !",
                Toast.LENGTH_SHORT
            ).show()
        }

        call.enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                // Handle this
                runOnUiThread {
                    Toast.makeText(this@CameraActivity, e.message, Toast.LENGTH_SHORT).show()
                    loading!!.visibility = View.GONE
                    startActivity(intent)
                }
            }

            override fun onResponse(call: Call, response: Response) {
                // Handle this
                runOnUiThread {
                    loading!!.visibility = View.GONE
                    var responseData : String = response.body!!.string()
                    var resObject = JSONObject(responseData)
                    if(resObject.getString("_status").equals("created")) {
                        Toast.makeText(
                            this@CameraActivity,
                            "Save entry successfully...",
                            Toast.LENGTH_SHORT
                        ).show()
                        var sSerchId =  resObject.getString("_id")
                        callNotificationMessage(sSerchId)
                    }else{
                        Toast.makeText(
                            this@CameraActivity,
                            "Save entry failed...",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                    finish()
                    startActivity(intent)
                }
            }
        })


    }



    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        //called when user presses ALLOW or DENY from Permission Request Popup
        when(requestCode){

            PERMISSION_ID -> {
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED)) {
                    // Granted. Start getting the location information
                }
            }

        }
    }


    @SuppressLint("MissingPermission")
    private fun getLastLocation()  {
        if (checkPermissions()) {
            if (isLocationEnabled()) {

                mFusedLocationClient.lastLocation.addOnCompleteListener(this@CameraActivity) { task ->
                    location = task.result!!
                    if (location == null) {
                        requestNewLocationData()
                    } else {
                        hashMap.put("location", location.latitude.toString()+","+location.longitude.toString())
                    }
                }
                mFusedLocationClient.lastLocation
                    .addOnSuccessListener { location->
                        if (location != null) {
                            // use your location object
                            // get latitude , longitude and other info from this
                            hashMap.put("location", location.latitude.toString()+","+location.longitude.toString())
                        }

                    }
            } else {
                Toast.makeText(this, "Turn on location", Toast.LENGTH_LONG).show()
                val intent = Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS)
                startActivity(intent)
            }
        } else {
            requestPermissions()
        }
    }

    @SuppressLint("MissingPermission")
    private fun requestNewLocationData() {
        var mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        mFusedLocationClient!!.requestLocationUpdates(
            mLocationRequest, mLocationCallback,
            Looper.myLooper()
        )
    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            location = locationResult.lastLocation
            hashMap.put("location", location.latitude.toString()+","+location.longitude.toString())
        }
    }

    private fun isLocationEnabled(): Boolean {
        var locationManager: LocationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER) || locationManager.isProviderEnabled(
            LocationManager.NETWORK_PROVIDER
        )
    }

    private fun checkPermissions(): Boolean {
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED &&
            ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) == PackageManager.PERMISSION_GRANTED
        ) {
            return true
        }
        return false
    }

    private fun requestPermissions() {
        ActivityCompat.requestPermissions(
            this,
            arrayOf(Manifest.permission.ACCESS_COARSE_LOCATION, Manifest.permission.ACCESS_FINE_LOCATION),
            PERMISSION_ID
        )
    }

    private fun setDate(v: EditText){
        c = Calendar.getInstance()
        val year = c.get(Calendar.YEAR)
        val month = c.get(Calendar.MONTH)
        val day = c.get(Calendar.DAY_OF_MONTH)

//        var dfs : DateFormatSymbols = DateFormatSymbols()
//        var months = dfs.months


        val dpd = DatePickerDialog(this@CameraActivity, DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            // Display Selected date in textbox
           // v.setText("" + dayOfMonth + " " + months[monthOfYear] + ", " + year)
            c.set(Calendar.YEAR, year)
            c.set(Calendar.MONTH, monthOfYear)
            c.set(Calendar.DAY_OF_MONTH,dayOfMonth)
            val timeSetListener = TimePickerDialog.OnTimeSetListener { timePicker, hour, minute ->
                c.set(Calendar.HOUR_OF_DAY, hour)
                c.set(Calendar.MINUTE, minute)
                v.setText(SimpleDateFormat("dd MMMM yyyy HH:mm a").format(c.time))
                var timeZone:TimeZone = TimeZone.getTimeZone("UTC")
                var gmtTime : String = c.time.formatTo("yyyy-MM-dd'T'HH:mm:ss",timeZone)
                hashMap.put("datetime", gmtTime)

            }
            TimePickerDialog(this, timeSetListener, c.get(Calendar.HOUR_OF_DAY), c.get(Calendar.MINUTE), true).show()
        }, year, month, day)

        dpd.show()
    }


    fun callNotificationMessage(smartSearchId : String){
        var userId = getValueString("userId")
        var userName = getValueString("userName")

        val payload = "{\"userId\": \""+ userId +"\",\n" +
                "\"type\": \"IMAGE_UPLOAD\",\n" +
                "\"msgType\": \"onTargetsNotification\",\n" +
                "\"message\": {\n" +
                "\"name\": \""+userName+"\",\n" +
                "\"userId\": \""+userId+"\",\n" +
                "\"searchId\": \""+smartSearchId+"\"\n" +
                "}\n" +
                "}"

        val okHttpClient = OkHttpClient()
        val requestBody = payload.toRequestBody()
        val request = Request.Builder()
            .method("POST", requestBody)
            .url(UrlUtils.NOTIFICATION_MESSAGE)
            .addHeader("Content-Type", "application/json")
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
                runOnUiThread {
                    Toast.makeText(this@CameraActivity,"Notification message" + e.message , Toast.LENGTH_SHORT).show()
                }
            }

            override fun onResponse(call: Call, response: Response) {
//                var responseData : String = response.body!!.string()
//                var resObject = JSONObject(responseData)
//                runOnUiThread {
//                    Toast.makeText(this@CameraActivity,"Notification message sent"  , Toast.LENGTH_SHORT).show()
//                }
            }
        })
    }

    fun callAuditTrial(fileName : String){
        var userId = getValueString("userId")
        var userName = getValueString("userName")
        var depId = getValueString("department")

        val data =   "{\"formName\": \"Upload\",\n" +
                "\"summary\": \""+ "Upload file" + fileName+"\",\n" +
                "\"action\": \"INSERT\",\n" +
                "\"userName\": \""+userName+"\",\n" +
                "\"userId\": \""+userId+"\",\n" +
                "\"depId\": \""+depId+"\",\n" +
                "\"original\": {\n" +
                "\"fileId\": \""+userName+"\"\n" +
                "}\n" +
                "}"

        val okHttpClient = OkHttpClient()
        val requestBody = data.toRequestBody()
        val request = Request.Builder()
            .method("POST", requestBody)
            .url(UrlUtils.AUDIT_TRAIL)
            .addHeader("Content-Type", "application/json")
            .build()
        okHttpClient.newCall(request).enqueue(object : Callback {
            override fun onFailure(call: Call, e: IOException) {
//                runOnUiThread {
//                    Toast.makeText(this@CameraActivity,"Notification message" + e.message , Toast.LENGTH_SHORT).show()
//                }
            }

            override fun onResponse(call: Call, response: Response) {
//                var responseData : String = response.body!!.string()
//                var resObject = JSONObject(responseData)
//                runOnUiThread {
//                    Toast.makeText(this@CameraActivity,"Notification message sent"  , Toast.LENGTH_SHORT).show()
//                }
            }
        })
    }

}


