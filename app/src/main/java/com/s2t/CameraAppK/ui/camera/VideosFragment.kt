package com.s2t.CameraAppK.ui.camera

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.*
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.media.MediaMetadataRetriever
import android.media.MediaScannerConnection
import android.media.ThumbnailUtils
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.Switch
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.loader.content.CursorLoader
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.s2t.CameraAppK.R
import com.s2t.CameraAppK.data.model.FileItem
import com.s2t.CameraAppK.ui.adapters.FileAdapter
import java.io.*
import java.lang.Exception
import java.util.*

class VideosFragment : Fragment() {
    private val PERMISSION_CODE = 2000
    private val VIDEO_CAPTURE_CODE = 2001
    private val REQUEST_SELECT_VIDEO_IN_ALBUM = 2002
    lateinit var myContext:Context
    var recyclerView : RecyclerView? = null
    lateinit var activity: CameraActivity
    lateinit var fileAdapter:FileAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        var view = inflater.inflate(R.layout.fragment_videos, container, false)
        recyclerView = view.findViewById(R.id.recyclerView)
        activity = context as CameraActivity
        val capture_btn = view.findViewById(R.id.capture_btn) as Button
        val browse_btn = view.findViewById(R.id.browse_btn) as Button

        capture_btn.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (myContext.checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED ||
                    myContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED){
                    //permission was not enabled
                    val permission = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    //show popup to request permission
                    requestPermissions(permission, PERMISSION_CODE)
                }
                else{
                    //permission already granted
                    openCamera()
                }
            }
            else{
                //system os is < marshmallow
                openCamera()
            }
        }

        browse_btn.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (myContext.checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED ||
                    myContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED){
                    //permission was not enabled
                    val permission = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    //show popup to request permission
                    requestPermissions(permission, PERMISSION_CODE)
                }
                else{
                    selectVideoInAlbum()
                }
            }
            else{
                selectVideoInAlbum()
            }
        }
        activity.videoToggle =  view.findViewById(R.id.togglebutton) as Switch
        recyclerView?.layoutManager = LinearLayoutManager(myContext, LinearLayoutManager.HORIZONTAL ,false)

        fileAdapter = FileAdapter(myContext,activity.videoFileItems, {fileItem : FileItem -> ItemLongClicked(fileItem)})

        recyclerView?.adapter = fileAdapter
        return view
    }

    fun selectVideoInAlbum() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "video/*"
        if (intent.resolveActivity(myContext.packageManager) != null) {
            startActivityForResult(intent, REQUEST_SELECT_VIDEO_IN_ALBUM)
        }
    }

    private fun ItemLongClicked(fileItem: FileItem): Boolean {

        val mAlertDialog = AlertDialog.Builder(myContext)
        mAlertDialog.setIcon(R.drawable.delete_bin) //set alertdialog icon
        mAlertDialog.setTitle("Remove?") //set alertdialog title
        mAlertDialog.setMessage("Do you want to remove : \n"+ fileItem.name +" ? ") //set alertdialog message
        mAlertDialog.setPositiveButton("Yes") { dialog, id ->
            //perform some tasks here
            activity.videoFileItems.remove(fileItem)
            fileAdapter.notifyDataSetChanged()
            activity.filename.setText(null)
            try {
                activity.filename.setText(activity.imageFileItems.get(0).name)
            }catch (e:Exception){
            }
            try {
                activity.filename.setText(activity.videoFileItems.get(0).name)
            }catch (e:Exception){
            }
            try {
                activity.filename.setText(activity.documentFileItems.get(0).name)
            }catch (e:Exception){
            }
            Toast.makeText(myContext, fileItem.name+ " is removed", Toast.LENGTH_SHORT).show()
        }
        mAlertDialog.setNegativeButton("No") { dialog, id ->
            //perform som tasks here
            Toast.makeText(myContext, fileItem.name+ " is still available", Toast.LENGTH_SHORT).show()
        }
        mAlertDialog.show()

        return false
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.myContext = context
    }

    private fun openCamera() {
        val cameraIntent = Intent(MediaStore.ACTION_VIDEO_CAPTURE)
        startActivityForResult(cameraIntent, VIDEO_CAPTURE_CODE)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        //called when user presses ALLOW or DENY from Permission Request Popup
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size > 0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup was granted
                    openCamera()
                }
                else{
                    //permission from popup was denied
                    Toast.makeText(myContext, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }

        }
    }


    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //called when image was captured from camera intent
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            if (data != null) {
                var contentURI = data.data

                try {
                    var fileItem: FileItem? = FileItem()
                    val uri:Uri = data.data!!
                    var contentResolver = myContext.contentResolver
                    var path = getRealPath(myContext, uri)
                    if(path.equals("null")){
                        path = contentURI!!.path.toString()
                    }
                    var file: File? = File(path)
                    var fileName = file?.name
                    fileItem?.type = "VIDEO"

                    if (fileName != null) {
                        fileItem?.name = fileName
                    }
                    fileItem?.file = file

                    if (fileItem != null) {
                        activity.videoFileItems.add(fileItem)
                    }
                    Toast.makeText(myContext, "Video Saved!", Toast.LENGTH_SHORT).show()
                    fileAdapter.notifyDataSetChanged()
                    try {
                        if (activity.filename.text.toString().isNullOrEmpty()) {
                            activity.filename.setText(fileAdapter.fileList.get(0).name)
                        }
                    }catch (e:Exception){

                    }

                }catch (e: Exception){
                    e.message
                }
            }
        }
    }



     fun getRealPath(context: Context, fileUri: Uri): String? {
        // SDK >= 11 && SDK < 19
        return if (Build.VERSION.SDK_INT < 19) {
            getRealPathFromURIAPI11to18(context, fileUri)
        } else {
            getRealPathFromURIAPI19(context, fileUri)
        }// SDK > 19 (Android 4.4) and up
    }

    @SuppressLint("NewApi")
    fun getRealPathFromURIAPI11to18(context: Context, contentUri: Uri): String? {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        var result: String? = null

        val cursorLoader = CursorLoader(context, contentUri, proj, null, null, null)
        val cursor = cursorLoader.loadInBackground()

        if (cursor != null) {
            val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            result = cursor.getString(columnIndex)
            cursor.close()
        }
        return result
    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri     The Uri to query.
     * @author Niks
     */
    @SuppressLint("NewApi")
    fun getRealPathFromURIAPI19(context: Context, uri: Uri): String? {

        val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }
                else {
                    var filePath: String? = null
                    if (Build.VERSION.SDK_INT > 20) {
                        //getExternalMediaDirs() added in API 21

                        var extenal= context.getExternalMediaDirs()
                        if (extenal.size > 1) {
                            filePath = extenal[1].getAbsolutePath()
                            filePath = filePath.substring(0, filePath.indexOf("Android")) + split[1]
                        }
                    }else{
                        filePath = "/storage/" + type + "/" + split[1]
                    }
                    return filePath
                }
            } else if (isDownloadsDocument(uri)) {
                var cursor: Cursor? = null
                try {
                    cursor = context.contentResolver.query(uri, arrayOf(MediaStore.MediaColumns.DISPLAY_NAME), null, null, null)
                    cursor!!.moveToNext()
                    val fileName = cursor.getString(0)
                    val path = Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName
                    if (!TextUtils.isEmpty(path)) {
                        return path
                    }
                } finally {
                    cursor?.close()
                }
                val id = DocumentsContract.getDocumentId(uri)
                if (id.startsWith("raw:")) {
                    return id.replaceFirst("raw:".toRegex(), "")
                }
                val contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads"), java.lang.Long.valueOf(id))

                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                var contentUri: Uri? = null
                when (type) {
                    "image" -> contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    "video" -> contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    "audio" -> contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }

                val selection = "_id=?"
                val selectionArgs = arrayOf(split[1])

                return getDataColumn(context, contentUri, selection, selectionArgs)
            }// MediaProvider
            // DownloadsProvider
        } else if ("content".equals(uri.scheme!!, ignoreCase = true)) {

            // Return the remote address
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(context, uri, null, null)
        } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
            return uri.path
        }// File
        // MediaStore (and general)

        return null
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context       The context.
     * @param uri           The Uri to query.
     * @param selection     (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     * @author Niks
     */
    private fun getDataColumn(context: Context, uri: Uri?, selection: String?,
                              selectionArgs: Array<String>?): String? {

        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)

        try {
            cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }
}




