package com.s2t.CameraAppK.ui.camera

import android.R.attr.path
import android.annotation.SuppressLint
import android.app.Activity
import android.content.ActivityNotFoundException
import android.content.ContentUris
import android.content.Context
import android.content.Intent
import android.database.Cursor
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.MimeTypeMap
import android.widget.Button
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.loader.content.CursorLoader
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.s2t.CameraAppK.R
import com.s2t.CameraAppK.data.model.FileItem
import com.s2t.CameraAppK.ui.adapters.FileAdapter
import java.io.File
import java.lang.Exception
import java.util.*


class DocumentFragment : Fragment() {
    private val PICKFILE_RESULT_CODE = 3002
    lateinit var myContext: Context
    var recyclerView : RecyclerView? = null
    lateinit var activity: CameraActivity
    lateinit var fileAdapter: FileAdapter
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        var view = inflater!!.inflate(R.layout.fragment_documents, container, false)
        activity = context as CameraActivity
        recyclerView = view.findViewById(R.id.recyclerView)
        val capture_btn = view.findViewById(R.id.capture_btn) as Button
        capture_btn.setOnClickListener {
            val mimeTypes = arrayOf(
                "application/msword",
                "application/vnd.openxmlformats-officedocument.wordprocessingml.document",  // .doc & .docx
                "application/vnd.ms-powerpoint",
                "application/vnd.openxmlformats-officedocument.presentationml.presentation",  // .ppt & .pptx
                "application/vnd.ms-excel",
                "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",  // .xls & .xlsx
                "application/vnd.oasis.opendocument.spreadsheet", // .ods
                "application/vnd.oasis.opendocument.text", // .odt
                "text/plain", // .txt
                "application/xhtml+xml", // .xhtml
                "application/xml",
                "text/xml", // .xml
                "text/html", // .html, .htm & .htmls
                "application/pdf"// .pdf
               // "application/zip" // .zip

            )

           // val mimeTypes = arrayOf("*/*")

            val fileintent = Intent(Intent.ACTION_GET_CONTENT)
            //fileintent.type = "*/*"
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
                fileintent.setType( mimeTypes[0])
                if (mimeTypes.size > 0) {
                    fileintent.putExtra(Intent.EXTRA_MIME_TYPES, mimeTypes)
                }
            } else {
                var mimeTypesStr = ""
                for (mimeType in mimeTypes) {
                    mimeTypesStr += "$mimeType|"
                }
                fileintent.setType(mimeTypesStr.substring(0, mimeTypesStr.length - 1))
            }
            try {
                startActivityForResult(fileintent, PICKFILE_RESULT_CODE)
            } catch (e: ActivityNotFoundException) {
                Log.e("tag", "No activity can handle picking a file. Showing alternatives.")
            }
        }
        recyclerView?.layoutManager = LinearLayoutManager(myContext, LinearLayoutManager.HORIZONTAL ,false)

        fileAdapter = FileAdapter(myContext,activity.documentFileItems, {fileItem : FileItem -> ItemLongClicked(fileItem)})

        //now adding the adapter to recyclerview
        recyclerView?.adapter = fileAdapter
        return view
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //called when image was captured from camera intent
        super.onActivityResult(requestCode, resultCode, data)
        // TODO Fix no activity available
        if (data == null)
            return;
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == PICKFILE_RESULT_CODE) {
                var filePath = data.data?.let { getRealPath(myContext, it) }
                //FilePath is your file as a string
                var file: File? = File(filePath)
                var fType = getMimeType(filePath)
                var fileItem: FileItem? = FileItem()
                fileItem?.file = file
                fileItem?.name = file?.name
                fileItem?.type = "DOCUMENT"
                val typeOfDoc = getMimeTypeName(filePath)
                val idName: Int
                if(typeOfDoc.equals("doc", true)|| typeOfDoc.equals("docx", true)){
                    idName = R.drawable.doc
                    fileItem?.thumb = idName
                }
                else if(typeOfDoc.equals("pdf", true)){
                    idName = R.drawable.pdf
                    fileItem?.thumb = idName
                }
                else if(typeOfDoc.equals("ppt", true)|| typeOfDoc.equals("pptx", true)){
                    idName = R.drawable.pptx
                    fileItem?.thumb = idName
                }
                else if(typeOfDoc.equals("xls", true)|| typeOfDoc.equals("xlsx", true)){
                    idName = R.drawable.xls
                    fileItem?.thumb = idName
                }
                else if(typeOfDoc.equals("htm", true)|| typeOfDoc.equals("html", true)|| typeOfDoc.equals("htmls", true)|| typeOfDoc.equals("xhtml", true)){
                    idName = R.drawable.html
                    fileItem?.thumb = idName
                }
                else if(typeOfDoc.equals("xml", true)){
                    idName = R.drawable.xml
                    fileItem?.thumb = idName
                }
                else if(typeOfDoc.equals("ods", true)){
                    idName = R.drawable.ods
                    fileItem?.thumb = idName
                }
                else if(typeOfDoc.equals("odt", true)){
                    idName = R.drawable.odt
                    fileItem?.thumb = idName
                }
                else if(typeOfDoc.equals("txt", true)){
                    idName = R.drawable.txt
                    fileItem?.thumb = idName
                }
                else{
                    idName = R.drawable.note
                    fileItem?.thumb = idName
                }
                fileItem?.let { activity.documentFileItems.add(it) }
                fileAdapter.notifyDataSetChanged()
                try {
                    if (activity.filename.text.toString().isNullOrEmpty()) {
                        activity.filename.setText(fileAdapter.fileList.get(0).name)
                    }
                }catch (e: Exception){

                }
            }
        }
    }


    fun getMimeTypeName(url: String?): String? {
        val extention: String = url!!.substring(url!!.lastIndexOf("."))
        val mimeTypeMap = MimeTypeMap.getFileExtensionFromUrl(extention)
//        val mimeType =
//            MimeTypeMap.getSingleton().getMimeTypeFromExtension(mimeTypeMap)
        return mimeTypeMap
    }

    fun getMimeType(url: String?): String? {
        val extention: String = url!!.substring(url!!.lastIndexOf("."))
        val mimeTypeMap = MimeTypeMap.getFileExtensionFromUrl(extention)
        val mimeType =
            MimeTypeMap.getSingleton().getMimeTypeFromExtension(mimeTypeMap)
        return mimeTypeMap
    }

    override fun onAttach(context: Context) {
        super.onAttach(context)
        this.myContext = context
    }

    private fun ItemLongClicked(fileItem: FileItem): Boolean {

        val mAlertDialog = AlertDialog.Builder(myContext)
        mAlertDialog.setIcon(R.drawable.delete_bin) //set alertdialog icon
        mAlertDialog.setTitle("Remove?") //set alertdialog title
        mAlertDialog.setMessage("Do you want to remove : \n"+ fileItem.name +" ? ") //set alertdialog message
        mAlertDialog.setPositiveButton("Yes") { dialog, id ->
            //perform some tasks here
            activity.documentFileItems.remove(fileItem)
            fileAdapter.notifyDataSetChanged()
            activity.filename.setText(null)
            try {
                activity.filename.setText(activity.imageFileItems.get(0).name)
            }catch (e:Exception){
            }
            try {
                activity.filename.setText(activity.videoFileItems.get(0).name)
            }catch (e:Exception){
            }
            try {
                activity.filename.setText(activity.documentFileItems.get(0).name)
            }catch (e:Exception){
            }
            Toast.makeText(myContext, fileItem.name+ " is removed", Toast.LENGTH_SHORT).show()
        }
        mAlertDialog.setNegativeButton("No") { dialog, id ->
            //perform som tasks here
            Toast.makeText(myContext, fileItem.name+ " is still available", Toast.LENGTH_SHORT).show()
        }
        mAlertDialog.show()

        return false
    }
    fun getRealPath(context: Context, fileUri: Uri): String? {
        // SDK >= 11 && SDK < 19
        return if (Build.VERSION.SDK_INT < 19) {
            getRealPathFromURIAPI11to18(context, fileUri)
        } else {
            getRealPathFromURIAPI19(context, fileUri)
        }// SDK > 19 (Android 4.4) and up
    }

    @SuppressLint("NewApi")
    fun getRealPathFromURIAPI11to18(context: Context, contentUri: Uri): String? {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        var result: String? = null

        val cursorLoader = CursorLoader(context, contentUri, proj, null, null, null)
        val cursor = cursorLoader.loadInBackground()

        if (cursor != null) {
            val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            result = cursor.getString(columnIndex)
            cursor.close()
        }
        return result
    }


    @SuppressLint("NewApi")
    fun getRealPathFromURIAPI19(context: Context, uri: Uri): String? {

        val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }
            } else if (isDownloadsDocument(uri)) {
                var cursor: Cursor? = null
                try {
                    cursor = context.contentResolver.query(uri, arrayOf(MediaStore.MediaColumns.DISPLAY_NAME), null, null, null)
                    cursor!!.moveToNext()
                    val fileName = cursor.getString(0)
                    val path = Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName
                    if (!TextUtils.isEmpty(path)) {
                        return path
                    }
                } finally {
                    cursor?.close()
                }
                val id = DocumentsContract.getDocumentId(uri)
                if (id.startsWith("raw:")) {
                    return id.replaceFirst("raw:".toRegex(), "")
                }
                val contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads"), java.lang.Long.valueOf(id))

                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                var contentUri: Uri? = null
                when (type) {
                    "image" -> contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    "video" -> contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    "audio" -> contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }

                val selection = "_id=?"
                val selectionArgs = arrayOf(split[1])

                return getDataColumn(context, contentUri, selection, selectionArgs)
            }// MediaProvider
            // DownloadsProvider
        } else if ("content".equals(uri.scheme!!, ignoreCase = true)) {

            // Return the remote address
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(context, uri, null, null)
        } else if ("file".equals(uri.scheme!!, ignoreCase = true)) {
            return uri.path
        }// File
        // MediaStore (and general)

        return null
    }


    private fun getDataColumn(context: Context, uri: Uri?, selection: String?,
                              selectionArgs: Array<String>?): String? {

        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)

        try {
            cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }


    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }

}
