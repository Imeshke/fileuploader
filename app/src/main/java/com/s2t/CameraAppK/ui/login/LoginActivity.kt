package com.s2t.CameraAppK.ui.login

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import android.os.Bundle
import android.os.StrictMode
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.EditText
import android.widget.ProgressBar
import android.widget.Toast
import com.beust.klaxon.JsonObject
import com.beust.klaxon.Parser

import com.s2t.CameraAppK.R
import com.s2t.CameraAppK.data.Result
import com.s2t.CameraAppK.data.model.LoggedInUser
import com.s2t.CameraAppK.data.utils.StringUtils
import com.s2t.CameraAppK.data.utils.UrlUtils
import com.s2t.CameraAppK.ui.camera.CameraActivity
import kotlinx.android.synthetic.main.activity_login.*
import okhttp3.*
import org.json.JSONException
import org.json.JSONObject
import java.io.IOException
import java.util.*

class LoginActivity : AppCompatActivity() {

    private lateinit var loginViewModel: LoginViewModel
    private var loading : ProgressBar? = null
    private val PREFS_NAME = "cam_shdprf"
    lateinit var sharedPref: SharedPreferences
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getSupportActionBar()!!.hide()
        setContentView(R.layout.activity_login)

        val policy =
            StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        sharedPref = getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE)

        val username: EditText = findViewById<EditText>(R.id.username)
        username.setText("optimus_admin")

        val password = findViewById<EditText>(R.id.password)
        password.setText("johnLock789_")

        val login = findViewById<Button>(R.id.login)
        loading= findViewById<ProgressBar>(R.id.loading)

        loginViewModel = ViewModelProviders.of(this, LoginViewModelFactory())
            .get(LoginViewModel::class.java)

        loginViewModel.loginFormState.observe(this@LoginActivity, Observer {
            val loginState = it ?: return@Observer

            // disable login button unless both username / password is valid
            login.isEnabled = loginState.isDataValid

            if (loginState.usernameError != null) {
                username.error = getString(loginState.usernameError)
            }
            if (loginState.passwordError != null) {
                password.error = getString(loginState.passwordError)
            }
        })

//        loginViewModel.loginResult.observe(this@LoginActivity, Observer {
//            val loginResult = it ?: return@Observer
//
//            loading.visibility = View.GONE
//            if (loginResult.error != null) {
//                showLoginFailed(loginResult.error)
//            }
//            if (loginResult.success != null) {
//                updateUiWithUser(loginResult.success)
//            }
//            setResult(Activity.RESULT_OK)
//
//            //Complete and destroy login activity once successful
//            finish()
//        })



        username.afterTextChanged {
            loginViewModel.loginDataChanged(
                username.text.toString(),
                password.text.toString()
            )
        }

        password.apply {
            afterTextChanged {
                loginViewModel.loginDataChanged(
                    username.text.toString(),
                    password.text.toString()
                )
            }

            setOnEditorActionListener { _, actionId, _ ->
                when (actionId) {
                    EditorInfo.IME_ACTION_DONE ->
                        loginViewModel.login(
                            username.text.toString(),
                            password.text.toString()
                        )
                }
                false
            }

            login.setOnClickListener {
                hideKeyboard()
                loading!!.visibility = View.VISIBLE
                login(username.text.toString(), password.text.toString())
            }
        }
    }

    fun saveShdPrf(KEY_NAME: String, value: String) {
        val editor: SharedPreferences.Editor = sharedPref.edit()

        editor.putString(KEY_NAME, value)

        editor.commit()
    }

    fun login(username : String, password : String){
        try {
            //loading!!.visibility = View.VISIBLE
            var responseObject: JSONObject
            var passwordSHA1 = StringUtils.SHA1(password)
            var appendsString = "?query=username:"+username+" AND password:"+passwordSHA1+"&index=user"

            GET(UrlUtils.LOGIN + appendsString, object: Callback {


                override fun onFailure(call: Call, e: IOException) {
                    runOnUiThread {
                        loading!!.visibility = View.GONE
                        showLoginFailed(e.localizedMessage);
                    }
                }

                override fun onResponse(call: Call, response: Response) {

                        var responseData : String = response.body!!.string()
                        runOnUiThread {
                            try {

                                loading!!.visibility = View.GONE
                                val resObject = JSONObject(responseData)
                                var totalHits:Number = resObject.getInt("TotalHits")
                                 if(totalHits!=0) {
                                     val document = resObject.getJSONArray("Documents")
                                     val user = document.getJSONObject(0)
                                     var username = user.getString("fullname")
                                     var uId = user.getString("_id")
                                     var department = user.getString("depRole")

                                     saveShdPrf("userId", uId)
                                     saveShdPrf("userName", username)
                                     saveShdPrf("department", department)

                                     //  val fakeUser = LoggedInUser(UUID.randomUUID().toString(), "display name")
                                     updateUiWithUser(username)
                                 }
                                else{
                                     showLoginFailed("Wrong username or password")
                                 }

                            } catch (e: JSONException) {
                                e.printStackTrace()
                            }
                        }

                }
            })

        } catch (e: Throwable) {

        }
    }

    var client = OkHttpClient()
    //var request = OkHttpRequest(client)
    fun GET(url: String, callback: Callback): Call {
        val request = Request.Builder()
            .url(url)
            .build()

        val call = client.newCall(request)
        call.enqueue(callback)
        return call
    }

    private fun updateUiWithUser(name : String) {
        val welcome = getString(R.string.welcome)
        val displayName = name
        // TODO : initiate successful logged in experience
        Toast.makeText(
            applicationContext,
            "$welcome $displayName",
            Toast.LENGTH_LONG
        ).show()
        intent = Intent(applicationContext, CameraActivity::class.java)
        intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_NEW_TASK
        startActivity(intent)

    }

    private fun showLoginFailed( errorString: String) {
        Toast.makeText(applicationContext, errorString, Toast.LENGTH_SHORT).show()
    }
}

fun View.hideKeyboard() {
    val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.hideSoftInputFromWindow(windowToken, 0)
}

/**
 * Extension function to simplify setting an afterTextChanged action to EditText components.
 */
fun EditText.afterTextChanged(afterTextChanged: (String) -> Unit) {
    this.addTextChangedListener(object : TextWatcher {
        override fun afterTextChanged(editable: Editable?) {
            afterTextChanged.invoke(editable.toString())
        }

        override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}

        override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    })




}



