package com.s2t.CameraAppK.ui.adapters

import android.content.Context
import androidx.fragment.app.Fragment

import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.s2t.CameraAppK.ui.camera.DocumentFragment
import com.s2t.CameraAppK.ui.camera.ImagesFragment
import com.s2t.CameraAppK.ui.camera.VideosFragment

class TabAdapter(private val myContext: Context, fm: FragmentManager, internal var totalTabs: Int) : FragmentPagerAdapter(fm) {

    // this is for fragment tabs
    override fun getItem(position: Int): Fragment {
        when (position) {
            0 -> {
                //  val homeFragment: HomeFragment = HomeFragment()
                return ImagesFragment()
            }
            1 -> {
                return VideosFragment()
            }
            2 -> {
                // val movieFragment = MovieFragment()
                return DocumentFragment()
            }
            else -> return ImagesFragment()
        }
    }

    // this counts total number of tabs
    override fun getCount(): Int {
        return totalTabs
    }
}