package com.s2t.CameraAppK.ui.camera

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.*
import android.content.pm.PackageManager
import android.database.Cursor
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.media.MediaScannerConnection
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.DocumentsContract
import android.provider.MediaStore
import android.text.TextUtils
import android.util.Log

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AlertDialog

import androidx.fragment.app.Fragment
import androidx.loader.content.CursorLoader
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.s2t.CameraAppK.R
import com.s2t.CameraAppK.data.model.FileItem
import com.s2t.CameraAppK.data.utils.ExifUtils
import com.s2t.CameraAppK.ui.adapters.FileAdapter
import kotlinx.android.synthetic.main.fragment_images.*
import java.io.*
import java.lang.Exception
import java.util.*
import kotlin.collections.ArrayList


class ImagesFragment() : Fragment() {
    private val PERMISSION_CODE = 1000;
    private val IMAGE_CAPTURE_CODE = 1001
    private val REQUEST_SELECT_IMAGE_IN_ALBUM = 1002
    lateinit var myContext:Context
    var recyclerView : RecyclerView? = null
    lateinit var activity: CameraActivity
    lateinit var fileAdapter:FileAdapter
    lateinit var image_uri: Uri

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        super.onCreateView(inflater, container, savedInstanceState)
        var view = inflater!!.inflate(R.layout.fragment_images, container, false)
        recyclerView = view.findViewById(R.id.recyclerView)
        activity = context as CameraActivity
        val capture_btn = view.findViewById(R.id.capture_btn) as Button
        val browse_btn = view.findViewById(R.id.browse_btn) as Button
        capture_btn.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (myContext.checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED ||
                    myContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED){
                    //permission was not enabled
                    val permission = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    //show popup to request permission
                    requestPermissions(permission, PERMISSION_CODE)
                }
                else{
                    //permission already granted
                    openCamera()
                }
            }
            else{
                //system os is < marshmallow
                openCamera()
            }
        }

        browse_btn.setOnClickListener {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
                if (myContext.checkSelfPermission(Manifest.permission.CAMERA)
                    == PackageManager.PERMISSION_DENIED ||
                    myContext.checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    == PackageManager.PERMISSION_DENIED){
                    //permission was not enabled
                    val permission = arrayOf(Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE)
                    //show popup to request permission
                    requestPermissions(permission, PERMISSION_CODE)
                }
                else{
                    //permission already granted
                    selectImageInAlbum()
                }
            }
            else{

                selectImageInAlbum()
            }
        }

        activity.imageToggle =  view.findViewById(R.id.togglebutton) as Switch

        recyclerView?.layoutManager = LinearLayoutManager(myContext, LinearLayoutManager.HORIZONTAL ,false)

        fileAdapter = FileAdapter(myContext,activity.imageFileItems, { fileItem : FileItem -> ItemLongClicked(fileItem)})

        recyclerView?.adapter = fileAdapter
        return view
    }



    private fun ItemLongClicked(fileItem: FileItem): Boolean {

        val mAlertDialog = AlertDialog.Builder(myContext)
        mAlertDialog.setIcon(R.drawable.delete_bin) //set alertdialog icon
        mAlertDialog.setTitle("Remove?") //set alertdialog title
        mAlertDialog.setMessage("Do you want to remove : \n"+ fileItem.name +" ? ") //set alertdialog message
        mAlertDialog.setPositiveButton("Yes") { dialog, id ->
            //perform some tasks here
            activity.imageFileItems.remove(fileItem)
            fileAdapter.notifyDataSetChanged()
            activity.filename.setText(null)
            try {
                activity.filename.setText(activity.imageFileItems.get(0).name)
            }catch (e:Exception){
            }
            try {
                activity.filename.setText(activity.videoFileItems.get(0).name)
            }catch (e:Exception){
            }
            try {
                activity.filename.setText(activity.documentFileItems.get(0).name)
            }catch (e:Exception){
            }
            Toast.makeText(myContext, fileItem.name+ " is removed", Toast.LENGTH_SHORT).show()
        }
        mAlertDialog.setNegativeButton("No") { dialog, id ->
            //perform som tasks here
            Toast.makeText(myContext, fileItem.name+ " is still available", Toast.LENGTH_SHORT).show()
        }
        mAlertDialog.show()

        return false
    }


    override fun onAttach(context: Context) {
        super.onAttach(context)
       this.myContext = context
    }


    fun selectImageInAlbum() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        if (intent.resolveActivity(myContext.packageManager) != null) {
            startActivityForResult(intent, REQUEST_SELECT_IMAGE_IN_ALBUM)
        }
    }

    private fun openCamera() {
        val values = ContentValues()
        values.put(MediaStore.Images.Media.TITLE, "New Picture")
        values.put(MediaStore.Images.Media.DESCRIPTION, "From the Camera")
        image_uri = myContext.contentResolver.insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, values)!!
        //camera intent
        val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
       // cameraIntent.putExtra(MediaStore.EXTRA_OUTPUT, image_uri)
        startActivityForResult(cameraIntent, IMAGE_CAPTURE_CODE)
    }


    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        //called when user presses ALLOW or DENY from Permission Request Popup
        when(requestCode){
            PERMISSION_CODE -> {
                if (grantResults.size > 0 && grantResults[0] ==
                    PackageManager.PERMISSION_GRANTED){
                    //permission from popup was granted
                    //openCamera()
                    Toast.makeText(myContext, "Permission granted", Toast.LENGTH_SHORT).show()
                }
                else{
                    //permission from popup was denied
                    Toast.makeText(myContext, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }

        }
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        //called when image was captured from camera intent
        super.onActivityResult(requestCode, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {

            if (requestCode == IMAGE_CAPTURE_CODE) {

                if (data != null) {
//                    var contentURI = data!!.data
//
//
//                       var  options = BitmapFactory.Options()
//                        options.inPreferredConfig = Bitmap.Config.ARGB_8888
//                        val bitmap = data.extras!!.get("data") as Bitmap

//                    fileItem?.thumb = bitmap
                try {
                    var fileItem: FileItem? = FileItem()
                    var bitmap = data.extras!!.get("data") as Bitmap
                    //fileItem?.thumb = bitmap
//                    var myBitmap : Bitmap = BitmapFactory.decodeStream(myContext.contentResolver.openInputStream(image_uri), null, null)!!
//                    var orientedBitmap:Bitmap = ExifUtils.rotateBitmap(getRealPath(myContext,image_uri), myBitmap)
//
                        var file: File? = null
                        file = saveImage(bitmap)
                        fileItem?.file = file!!
                        fileItem?.type = "IMAGE"
                        Toast.makeText(myContext, "Image Saved!", Toast.LENGTH_SHORT).show()
                        var fileName = file?.absoluteFile?.name
                        //filename.setText(fileName)
                        if (fileName != null) {
                            fileItem?.name = fileName
                        }
                        if (fileItem != null) {
                            activity.imageFileItems.add(fileItem)
                        }

                        fileAdapter.notifyDataSetChanged()
                        try {
                            if (activity.filename.text.toString().isNullOrEmpty()) {
                                activity.filename.setText(fileAdapter.fileList.get(0).name)
                            }
                        }catch (e:Exception){

                        }

                    } catch (e: Exception) {
                        e.message
                    }
                }
            }
            if(requestCode == REQUEST_SELECT_IMAGE_IN_ALBUM){
               var contentURI = data!!.data

                var filePath:String

                var fileItem: FileItem? = FileItem()

                filePath = getRealPath(myContext, contentURI!!).toString()

                var file: File? = File(filePath)

                fileItem?.file = file
                fileItem?.name = file?.absoluteFile?.name
                fileItem?.type = "IMAGE"
                if (fileItem != null) {
                    activity.imageFileItems.add(fileItem)
                }
                fileAdapter.notifyDataSetChanged()
                try {
                    if (activity.filename.text.toString().isNullOrEmpty()) {
                        activity.filename.setText(fileAdapter.fileList.get(0).name)
                    }
                }catch (e:Exception){

                }

            }

        }
    }

    fun saveImage(myBitmap: Bitmap): File? {
        val bytes = ByteArrayOutputStream()
        myBitmap.compress(Bitmap.CompressFormat.JPEG, 100, bytes)
        val wallpaperDirectory = File(
            (Environment.getExternalStorageDirectory()).toString() )
        // have the object build the directory structure, if needed.
        Log.d("fee",wallpaperDirectory.toString())
        if (!wallpaperDirectory.exists())
        {

            wallpaperDirectory.mkdirs()
        }

        try
        {
            // Log.d("heel",wallpaperDirectory.toString())
            val f = File(wallpaperDirectory, ((Calendar.getInstance()
                .getTimeInMillis()).toString() + ".jpg"))
            f.createNewFile()
            val fo = FileOutputStream(f)
            fo.write(bytes.toByteArray())
            MediaScannerConnection.scanFile(myContext,
                arrayOf(f.getPath()),
                arrayOf("image/jpeg"), null)
            fo.close()
            Log.d("TAG", "File Saved::--->" + f.getAbsolutePath())

            return f
        }
        catch (e1: IOException) {
            e1.printStackTrace()
        }

        return null
    }


}

    fun getRealPath(context: Context, fileUri: Uri): String? {
        // SDK >= 11 && SDK < 19
        return if (Build.VERSION.SDK_INT < 19) {
            getRealPathFromURIAPI11to18(context, fileUri)
        } else {
            getRealPathFromURIAPI19(context, fileUri)
        }// SDK > 19 (Android 4.4) and up
    }

    @SuppressLint("NewApi")
    fun getRealPathFromURIAPI11to18(context: Context, contentUri: Uri): String? {
        val proj = arrayOf(MediaStore.Images.Media.DATA)
        var result: String? = null

        val cursorLoader = CursorLoader(context, contentUri, proj, null, null, null)
        val cursor = cursorLoader.loadInBackground()

        if (cursor != null) {
            val columnIndex = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA)
            cursor.moveToFirst()
            result = cursor.getString(columnIndex)
            cursor.close()
        }
        return result
    }


    @SuppressLint("NewApi")
    fun getRealPathFromURIAPI19(context: Context, uri: Uri): String? {

        val isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                if ("primary".equals(type, ignoreCase = true)) {
                    return Environment.getExternalStorageDirectory().toString() + "/" + split[1]
                }
                else {
                    var filePath: String? = null
                    if (Build.VERSION.SDK_INT > 20) {
                        //getExternalMediaDirs() added in API 21

                        var extenal= context.getExternalMediaDirs()
                        if (extenal.size > 1) {
                            filePath = extenal[1].getAbsolutePath()
                            filePath = filePath.substring(0, filePath.indexOf("Android")) + split[1]
                        }
                    }else{
                        filePath = "/storage/" + type + "/" + split[1]
                    }
                    return filePath
                }
            } else if (isDownloadsDocument(uri)) {
                var cursor: Cursor? = null
                try {
                    cursor = context.contentResolver.query(uri, arrayOf(MediaStore.MediaColumns.DISPLAY_NAME), null, null, null)
                    cursor!!.moveToNext()
                    val fileName = cursor.getString(0)
                    val path = Environment.getExternalStorageDirectory().toString() + "/Download/" + fileName
                    if (!TextUtils.isEmpty(path)) {
                        return path
                    }
                } finally {
                    cursor?.close()
                }
                val id = DocumentsContract.getDocumentId(uri)
                if (id.startsWith("raw:")) {
                    return id.replaceFirst("raw:".toRegex(), "")
                }
                val contentUri = ContentUris.withAppendedId(Uri.parse("content://downloads"), java.lang.Long.valueOf(id))

                return getDataColumn(context, contentUri, null, null)
            } else if (isMediaDocument(uri)) {
                val docId = DocumentsContract.getDocumentId(uri)
                val split = docId.split(":".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()
                val type = split[0]

                var contentUri: Uri? = null
                when (type) {
                    "image" -> contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI
                    "video" -> contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI
                    "audio" -> contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI
                }

                val selection = "_id=?"
                val selectionArgs = arrayOf(split[1])

                return getDataColumn(context, contentUri, selection, selectionArgs)
            }// MediaProvider
            // DownloadsProvider
        }
        if ("content".equals(uri.scheme!!, ignoreCase = true)) {

            // Return the remote address
            return if (isGooglePhotosUri(uri)) uri.lastPathSegment else getDataColumn(context, uri, null, null)
        }
        if ("file".equals(uri.scheme!!, ignoreCase = true)) {
            return uri.path
        }// File
        // MediaStore (and general)

        return null
    }


    private fun getDataColumn(context: Context, uri: Uri?, selection: String?,
                              selectionArgs: Array<String>?): String? {

        var cursor: Cursor? = null
        val column = "_data"
        val projection = arrayOf(column)

        try {
            cursor = context.contentResolver.query(uri!!, projection, selection, selectionArgs, null)
            if (cursor != null && cursor.moveToFirst()) {
                val index = cursor.getColumnIndexOrThrow(column)
                return cursor.getString(index)
            }
        } finally {
            cursor?.close()
        }
        return null
    }


    private fun isExternalStorageDocument(uri: Uri): Boolean {
        return "com.android.externalstorage.documents" == uri.authority
    }

    private fun isDownloadsDocument(uri: Uri): Boolean {
        return "com.android.providers.downloads.documents" == uri.authority
    }

    private fun isMediaDocument(uri: Uri): Boolean {
        return "com.android.providers.media.documents" == uri.authority
    }

    private fun isGooglePhotosUri(uri: Uri): Boolean {
        return "com.google.android.apps.photos.content" == uri.authority
    }





