package com.s2t.CameraAppK.data.utils

internal object UrlUtils {
    var LOGIN = "https://cybersmart.predictintel.ai/managementModule/api/documents/"
    var SERVER_POST_FILES = "https://cybersmart.predictintel.ai/docserver/files/"
    var SERVER_POST_DATA = "https://cybersmart.predictintel.ai/docserver/docs/ImageUploads"
    var NOTIFICATION_MESSAGE = "https://cybersmart.predictintel.ai/managementModule/api/notifications/send"
    var AUDIT_TRAIL = "https://cybersmart.predictintel.ai/docserver/docs/AuditTrail"
}